<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 4/6/15
 * Time: 8:58 AM
 * To change this template use File | Settings | File Templates.
 */

class FileExtension extends DataExtension
{

	private static $db = array(
		'Credit'			=> 'Varchar(500)',
		'Caption'			=> 'Varchar(500)'
	);



	public function updateCMSFields(FieldList $fields)
	{

		if(!($this->owner instanceof Folder)) {
			$fields->addFieldsToTab('Root.Main', array(
				TextareaField::create('Credit')->setValue($this->owner->Credit),
				TextareaField::create('Caption')->setValue($this->owner->Caption)
			));
		}

	}

	public function SpaceHeight()
	{
		return ($this->owner->getHeight() / $this->owner->getWidth()) * 100;
	}

	public function AbsoluteLinkWithNoProtocol()
	{
		$url = $this->owner->AbsoluteLink();
		return str_replace('http:', '', str_replace('https:', '', $url));
	}

} 