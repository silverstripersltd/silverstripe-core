<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 4/6/15
 * Time: 9:36 AM
 * To change this template use File | Settings | File Templates.
 */

class CoreConfigs extends DataExtension {

	private static $db = array(
		'JSHeader'				=> 'Text',
		'JSAtBodyStart'			=> 'Text',
		'JSAtBodyEnd'			=> 'Text',
	);
	

	public function updateCMSFields(FieldList $fields)
	{

		$fields->addFieldsToTab('Root.Settings.EmbedScripts', array(
			TextareaField::create('JSHeader')->setTitle('JS in the head section'),
			TextareaField::create('JSAtBodyStart')->setTitle('JS at the start of the body (GMT tracking codes etc)'),
			TextareaField::create('JSAtBodyEnd')->setTitle('JS at the end of the body (Google analytics etc)'),
		));

	}


} 