<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 6/12/15
 * Time: 8:33 AM
 * To change this template use File | Settings | File Templates.
 */

class ControllerExtensions extends Extension {



	/**
	 * @return bool
	 */
	function HasCustomSecurityTemplate()
	{
		return SSViewer::hasTemplate(array(
			'CustomSecurity'
		));
	}


	/**
	 * @return bool
	 */
	public function IsDev()
	{
		return Director::isDev();
	}

	public function ThemedImage($imagePath)
	{
		$path = BASE_PATH . '/' . SSViewer::get_theme_folder() . '/' . $imagePath;
		if(file_exists($path)) {
			$image = new ResponsiveImage_Cached(SSViewer::get_theme_folder() . '/' . $imagePath);
			return $image;
		}
	}


	public function ThemedSVG($imagePath)
	{
		$path = BASE_PATH . '/' . SSViewer::get_theme_folder() . '/' . $imagePath;
		if(file_exists($path)) {
			return file_get_contents($path);
		}
	}

} 