<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 4/6/15
 * Time: 8:53 AM
 * To change this template use File | Settings | File Templates.
 */

class Page extends SiteTree {

	private $processedDom = '';
	private static $disable_meta = false;

	private static $db = array(
		'MetaTitle'				=> 'Varchar(255)',
		'RemoveFromSiteMap'		=> 'Boolean'
	);

	function canView($member = null){
		if($this->URLSegment == 'SearchPage')
			return true;
		return parent::canView($member);
	}

	function canViewStage($stage = 'Live', $member = null){
		if($this->URLSegment == 'SearchPage')
			return true;

		return parent::canViewStage($stage, $member);
	}

	function getCMSFields(){
		$fields = parent::getCMSFields();

		$metaData = $fields->fieldByName('Root.Main.Metadata');
		if($metaData)
			$metaData->push(new TextField('MetaTitle', 'Meta title'));
		else
			$fields->addFieldToTab("Root.Main", new TextField('MetaTitle', 'Meta title'));

		$this->extend('updateCMSFieldsForImages', $fields);

		return $fields;
	}


	function getSettingsFields()
	{
		$fields = parent::getSettingsFields();
		$fields->insertAfter('ShowInSearch', CheckboxField::create('RemoveFromSiteMap', 'Remove from from sitemap'));
		return $fields;
	}

	function RootPage(){
		if($this->ParentID == 0)
			return $this;
		return $this->Parent()->RootPage();
	}

	public function MetaTags($includeTitle = true)
	{

		$tags = "";
		if(!self::disable_meta()) {
			if($includeTitle === true || $includeTitle == 'true') {
				$tags .= "<title>" . Convert::raw2xml($this->Title) . "</title>\n";
			}

			$tags .= "<meta name=\"generator\" content=\"SilverStripe - http://www.silverstripers.com/\" />\n";

			$charset = Config::inst()->get('ContentNegotiator', 'encoding');
			$tags .= "<meta http-equiv=\"Content-type\" content=\"text/html; charset=$charset\" />\n";
			if($this->MetaTitle) {
				$tags .= "<meta name=\"title\" content=\"" . Convert::raw2att($this->MetaTitle) . "\" />\n";
			}
			else if ($processedMetaTitle = $this->findMetaTitleFromDom()){
				$tags .= "<meta name=\"title\" content=\"" . Convert::raw2att($processedMetaTitle) . "\" />\n";
			}
			if($this->MetaDescription) {
				$tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->MetaDescription) . "\" />\n";
			}
			else if ($processedMetaDesc = $this->findMetaDescriptionFromDom()) {
				$tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($processedMetaDesc) . "\" />\n";
			}

			
			if($this->ExtraMeta) {
				$tags .= $this->ExtraMeta . "\n";
			}

			$this->getDom();

			if(Permission::check('CMS_ACCESS_CMSMain')
				&& in_array('CMSPreviewable', class_implements($this))
				&& !$this instanceof ErrorPage
				&& $this->ID > 0
			) {
				$tags .= "<meta name=\"x-page-id\" content=\"{$this->ID}\" />\n";
				$tags .= "<meta name=\"x-cms-edit-link\" content=\"" . $this->CMSEditLink() . "\" />\n";
			}

			$this->extend('MetaTags', $tags);
		}

		return $tags;
	}

	public static function set_disable_meta($disable = true)
	{
		self::$disable_meta = $disable;
	}

	public static function disable_meta()
	{
		return self::$disable_meta;
	}

	public function getDom()
	{
		if(empty($this->processedDom)) {
			Page::set_disable_meta();
			$templates = array_merge(
				SSViewer::get_templates_by_class(get_class($this), "", "SiteTree"),
				SSViewer::get_templates_by_class(get_class($this), "", "Controller")
			);
			$viewer = new SSViewer($templates);
			$this->processedDom = $this->renderWith($viewer);
			Page::set_disable_meta(false);
		}
		return $this->processedDom;
	}

	public function findMetaTitleFromDom()
	{
		$dom = $this->getDom();
		$parsedTitle = StringUtils::GetBeforeBetweenAndAfter($dom, '<h1', '</h1>');
		if($parsedTitle) {
			return substr($parsedTitle['Between'], strpos($parsedTitle['Between'], '>') + 1);
		}
	}

	public function findMetaDescriptionFromDom()
	{
		$dom = $this->getDom();
		$parsedTitle = StringUtils::GetBeforeBetweenAndAfter($dom, '<h2', '</h2>');
		if($parsedTitle) {
			return substr($parsedTitle['Between'], strpos($parsedTitle['Between'], '>') + 1);
		}
	}


}

class Page_Controller extends ContentController {

	public function init()
	{
		parent::init();
	}

	function Year(){
		return date('Y', strtotime(SS_Datetime::now()));
	}

	function SearchPageLink(){
		if($page = SearchPage::get()->first()){
			return $page->Link();
		}
		return Director::baseURL() . 'search';
	}

	public function IsDev()
	{
		return Director::isDev();
	}

	public function TimeStamp()
	{
		return time();
	}



}