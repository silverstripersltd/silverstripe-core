<?php

/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 6/8/16
 * Time: 10:33 AM
 * To change this template use File | Settings | File Templates.
 */
class CustomSiteMapPage extends Page
{

}

class CustomSiteMapPage_Controller extends Page_Controller
{


	public function GetList()
	{
		$list = $this->MakeListArray();
		return $this->MakeUL($list);
	}


	public function MakeUL($list)
	{
		$html = '<ul>';

		foreach($list as $page) {
			if(!empty($page['Children'])) {
				$html .= "<li class='children'>";
			}
			else {
				$html .= "<li>";
			}
			$html.= "<a href='" . $page['Link'] . "'>" . $page['MenuTitle'] . "</a>";
			if(!empty($page['Children'])) {
				$html.= $this->MakeUL($page['Children']);
			}
			$html.= "</li>";
		}

		$html.= '</ul>';
		return $html;
	}

	public function MakeListArray($parent = 0)
	{
		$pages = Page::get()->filter(array(
			'ParentID'								=> $parent,
			'ClassName:not'							=> 'ErrorPage',
			'RemoveFromSiteMap'						=> 0
		));

		$list = array();

		foreach($pages as $page) {
			$list[] = array(
				'MenuTitle'			=> $page->MenuTitle,
				'Link'				=> $page->Link(),
				'Children'			=> $this->MakeListArray($page->ID)
			);
		}


		return $list;

	}

}