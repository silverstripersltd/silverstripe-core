<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 4/6/15
 * Time: 8:59 AM
 * To change this template use File | Settings | File Templates.
 */

class FormUtils {


	/**
	 * @param $strName
	 * @param $strLabel
	 * @param $strValue
	 * @return LiteralField
	 */
	public static function GetCMSFieldTypeLiteralField($strName, $strLabel, $strValue){
		return new LiteralField($strName, "<div class='field'>
			<label class='left'>{$strLabel}</label>
			<div class='middleColumn'>
				<p>$strValue</p>
			</div>
		</div>");
	}


	
	/**
	 * @param $strCSV
	 * @return array
	 */
	public static function CSVToSourceArray($strCSV){
		$arrRet = array();
		$arrItems = explode(',', $strCSV);

		foreach($arrItems as $strVal){
			$strVal = trim($strVal);
			$arrRet[$strVal] = $strVal;
		}

		return $arrRet;
	}





} 