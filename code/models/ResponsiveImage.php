<?php

/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 7/28/16
 * Time: 6:23 PM
 * To change this template use File | Settings | File Templates.
 */


class ResponsiveImage extends Image
{

	/**
	 * @param int $width
	 * @param int $height
	 * @return Image|null
	 * Focus point support
	 */
	public function Fill($width, $height)
	{
		if(method_exists($this, 'FocusFill')) {
			return $this->FocusFill($width, $height);
		}
		return parent::Fill($width, $height);
	}

	public function FillMax($width, $height)
	{
		if(method_exists($this, 'FocusFillMax')) {
			return $this->FocusFillMax($width, $height);
		}
		return parent::FillMax($width, $height);
	}

	public function CropWidth($width)
	{
		if(method_exists($this, 'FocusCropWidth')) {
			return $this->FocusCropWidth($width);
		}
		return parent::CropWidth($width);

	}

	public function CropHeight($height)
	{
		if(method_exists($this, 'FocusCropHeight')) {
			return $this->FocusCropHeight($height);
		}
		return parent::CropWidth($height);
	}

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();

		if($this->ClassName != 'ResponsiveImage') {
			$fields->removeByName('FocusPoint');
		}

		return $fields;
	}


	//
	// 1400_min_700_700,1022_min_511_511|1022_max_1022_1022|768_max_768_768|330_max_320_320
	//
	public function CroppedResponsiveImage($pattern)
	{
		$sizes = explode("|", $pattern);
		$images = new ArrayList();
		foreach ($sizes as $size) {
			$detail = explode('_', $size);
			if(count($detail) == 4) {
				$screen = $detail[0];
				$mediaQuery = 'max-width: ' . $screen . 'px';
				if($detail[1] == 'min') {
					$screen += 1;
					$mediaQuery = 'min-width: ' . $screen . 'px';
				}

				$resampled = $this;
				if(($detail[2] != 0 && $detail[3] != 0)) {
					if(ResponsiveImage::has_extension('FocusPointImage')) {
						$resampled = $this->FocusFill($detail[2], $detail[3]);
					}
					else {
						$resampled = $this->Fill($detail[2], $detail[3]);
					}
				}


				$images->push(new ArrayData(array(
					'ScreenWith'	=> $screen,
					'Image'			=> $resampled, // $this->Fill($detail[2], $detail[3]),
					'MediaQuery'	=> $mediaQuery,
					'MediaQueryType'=> $detail[1]
				)));
			}

		}

		$images = $images->sort('ScreenWith DESC');

		if($responsiveFirst  = $images->first()){
			if($responsiveFirst->Image){
				$image = $responsiveFirst->Image;
				$image->ResponsiveImages = $images;
				return $image;
			}
		}

	}


	//
	public function SetWidthResponsiveImage($pattern)
	{
		$sizes = explode("|", $pattern);
		$images = new ArrayList();
		foreach ($sizes as $size) {
			$detail = explode('_', $size);
			if(count($detail) == 3) {
				$screen = $detail[0];
				$mediaQuery = 'max-width: ' . $screen . 'px';
				if($detail[1] == 'min') {
					$screen += 1;
					$mediaQuery = 'min-width: ' . $screen . 'px';
				}

				$resampled = ($detail[2] != 0) ? $this->ScaleWidth($detail[2]) : $this;

				$images->push(new ArrayData(array(
					'ScreenWith'	=> $screen,
					'Image'			=> $resampled, // $this->Fill($detail[2], $detail[3]),
					'MediaQuery'	=> $mediaQuery,
					'MediaQueryType'=> $detail[1]
				)));
			}

		}

		$images = $images->sort('ScreenWith DESC');

		if($responsiveFirst  = $images->first()){
			if($responsiveFirst->Image){
				$image = $responsiveFirst->Image;
				$image->ResponsiveImages = $images;
				return $image;
			}
		}

	}



	//
	//
	//


	public function getFormattedImage($format)
	{
		$args = func_get_args();
		if($this->exists()) {
			$cacheFile = call_user_func_array(array($this, "cacheFilename"), $args);
			if(!file_exists(Director::baseFolder()."/".$cacheFile) || self::flush()) {
				call_user_func_array(array($this, "generateFormattedImage"), $args);
			}
			$cached = new ResponsiveImage_Cached($cacheFile, false, $this);
			return $cached;
		}
	}

	public function getTag() {
		if($this->exists()) {
			$controller = Controller::has_curr() ? Controller::curr() : null;
			if($controller && is_a($controller, 'LeftAndMain')) {
				return parent::getTag();
			}


			$url = $this->getURL();
			$title = ($this->Title) ? $this->Title : $this->Filename;
			if($this->Title) {
				$title = Convert::raw2att($this->Title);
			} else {
				if(preg_match("/([^\/]*)\.[a-zA-Z0-9]{1,6}$/", $title, $matches)) {
					$title = Convert::raw2att($matches[1]);
				}
			}

			$width = $this->getWidth();
			$height = $this->getHeight();
			$caption = $this->caption;
			$credit = $this->credit;

			
			return $this->customise(array(
				'Width'			=> $width,
				'Height'		=> $height,
				'Caption'		=> $caption,
				'Credit'		=> $credit,
				'URL'			=> $url,
				'Title'			=> $title,
				'SpaceHeight'	=> $this->SpaceHeight()
			))->renderWith('ImageHTML');
		}
	}

}


class ResponsiveImage_Cached extends ResponsiveImage
{

	/**
	 * Create a new cached image.
	 * @param string $filename The filename of the image.
	 * @param boolean $isSingleton This this to true if this is a singleton() object, a stub for calling methods.
	 *                             Singletons don't have their defaults set.
	 */
	public function __construct($filename = null, $isSingleton = false, Image $sourceImage = null) {
		parent::__construct(array(), $isSingleton);
		if ($sourceImage) {
			// Copy properties from source image, except unsafe ones
			$properties = $sourceImage->toMap();
			unset($properties['RecordClassName'], $properties['ClassName']);
			$this->update($properties);
		}
		$this->ID = -1;
		$this->Filename = $filename;
	}

	/**
	 * Override the parent's exists method becuase the ID is explicitly set to -1 on a cached image we can't use the
	 * default check
	 *
	 * @return bool Whether the cached image exists
	 */
	public function exists() {
		return file_exists($this->getFullPath());
	}

	public function getRelativePath() {
		return $this->getField('Filename');
	}

	/**
	 * Prevent creating new tables for the cached record
	 *
	 * @return false
	 */
	public function requireTable() {
		return false;
	}

	/**
	 * Prevent writing the cached image to the database
	 *
	 * @throws Exception
	 */
	public function write($showDebug = false, $forceInsert = false, $forceWrite = false, $writeComponents = false) {
		throw new Exception("{$this->ClassName} can not be written back to the database.");
	}

}