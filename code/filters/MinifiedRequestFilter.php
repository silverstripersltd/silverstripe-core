<?php

/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 7/28/16
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
class MinifiedRequestFilter implements RequestFilter
{

	private static $minify_all = false;

	private $remove_comments = true;
	private $compress_js = true;
	private $compress_css = true;


	public function preRequest(SS_HTTPRequest $request, Session $session, DataModel $model)
	{
		return true;
	}

	public function postRequest(SS_HTTPRequest $request, SS_HTTPResponse $response, DataModel $model)
	{
		$minify = false;
		$controller = $response->getHeader('X-Controller');
		if($controller) {
			$contentControllers = ClassInfo::subclassesFor('ContentController');
			if(in_array($controller, $contentControllers)) {
				$minify = true;
			}
		}
		else {
			$minify = true;
		}

		if($minify) {
			if (Director::isLive() || Config::inst()->get('MinifiedRequestFilter', 'minify_all')) {
				$body = $response->getBody();
				//$response->setBody($this->minify($body));
			}
		}
	}

	public function minify($html)
	{


		$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
		preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
		$overriding = false;
		$raw_tag = false;
		$html = '';
		foreach ($matches as $token) {
			$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
			$content = $token[0];
			if (is_null($tag)) {
				if ( !empty($token['script']) ) {
					$strip = $this->compress_js;
				}
				else if ( !empty($token['style']) ) {
					$strip = $this->compress_css;
				}
				else if ($content == '<!--wp-html-compression no compression-->') {
					$overriding = !$overriding;
					continue;
				}
				else if ($this->remove_comments) {
					if (!$overriding && $raw_tag != 'textarea') {
						$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
					}
				}
			}
			else {
				if ($tag == 'pre' || $tag == 'textarea') {
					$raw_tag = $tag;
				}
				else if ($tag == '/pre' || $tag == '/textarea') {
					$raw_tag = false;
				}
				else {
					if ($raw_tag || $overriding) {
						$strip = false;
					}
					else {
						$strip = true;
						$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
						$content = str_replace(' />', '/>', $content);
					}
				}
			}
			if ($strip) {
				$content = $this->removeWhiteSpace($content);
			}
			$html .= $content;
		}
		return $html;

	}

	public function removeWhiteSpace($str)
	{
		$str = str_replace("\t", ' ', $str);
		$str = str_replace("\n",  '', $str);
		$str = str_replace("\r",  '', $str);
		while (stristr($str, '  ')) {
			$str = str_replace('  ', ' ', $str);
		}
		return $str;
	}

}