<?php

/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 7/28/16
 * Time: 11:46 AM
 * To change this template use File | Settings | File Templates.
 */
class ScriptsRequestFilters implements RequestFilter
{


	public function preRequest(SS_HTTPRequest $request, Session $session, DataModel $model)
	{
		return true;
	}

	public function postRequest(SS_HTTPRequest $request, SS_HTTPResponse $response, DataModel $model)
	{
		if($response->getStatusCode() == 200) {
			$body = $response->getBody();

			$config = SiteConfig::current_site_config();


			$headTag = StringUtils::GetBeforeBetweenAndAfter($body, '<head>', '</head>');
			if($config->JSHeader && $headTag) {
				$body = $headTag['Before']
					. '<head>'
					. $headTag['Between']
					. $config->JSHeader . "\n\n"
					. "</head>\n\n"
					. $headTag['After'];
			}


			$bodyTag = StringUtils::GetBeforeBetweenAndAfter($body, '<body', '>');
			if($config->JSAtBodyStart && $bodyTag) {
				$body = $bodyTag['Before']
					. '<body'
					. $bodyTag['Between']
					. ">\n\n"
					. "<div style='display: none'>"
					. $config->JSAtBodyStart . "\n\n"
					. "</div>"
					. $bodyTag['After'];
			}


			$bodyEndTag = StringUtils::GetBeforeBetweenAndAfter($body, '</body', '>');
			if($config->JSAtBodyEnd && $bodyTag) {
				$body = $bodyEndTag['Before']
					. '</body>'
					. $config->JSAtBodyEnd
					. $bodyEndTag['After'];
			}

			$response->setBody($body);

		}




	}


}